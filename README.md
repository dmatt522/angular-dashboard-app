# The dashboard website for mymobilewatchdog.com
###### The AngularJS application running on the simple web server using Python or Node.
***

### Build App ###

1. npm install
2. bower install
3. grunt (for test build)
4. grunt prod (for production build)

To launch a simple local web server using python

```shell
python -m SimpleHTTPServer 12493
```

To launch a simple local web server using node

```javascript
var connect = require('connect');
var serveStatic = require('serve-static');
connect().use(serveStatic(__dirname)).listen(12493);
```

#### Confirm Box ####
Allows the creation of a confirmation modal box based on [$uibModal](https://angular-ui.github.io/bootstrap/#/modal).
The functions return a promise where the catch is called when the user clicks the _Negative_ button.

```javascript
confirmBox.showYesNo('My Title', 'My confirm box text')
    .then(function(response) {
        if(response) {
            // User clicked yes
        }
    })
    .catch(function(err) {
        // User clicked no
    })
```