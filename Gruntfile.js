module.exports = function (grunt) {
    grunt.initConfig({
        copy: {
            main: {
                files: [
                    {
                        cwd: 'bower_components',
                        src: [
                            'angular-*/**/angular-*.min.js',
                            'angular-*/**/angular-*.min.css',
                            'angular/angular.min.js',
                            'angular-bootstrap/ui-*.min.js',
                            'bluebird/js/browser/bluebird.min.js',
                            'ng-file-upload/ng-file-upload-all.min.js',
                            'lodash/lodash.min.js',
                            'ui-leaflet/dist/ui-leaflet.min.js',
                            'ng-timeago/ngtimeago.js',
                            'ngInfiniteScroll/build/ng-infinite-scroll.min.js',
                            'moment/min/moment.min.js',
                            'fullcalendar/dist/*.min.*',
                            'html5-boilerplate/dist/js/vendor/modernizr-2.8.3.min.js',
                            'angulartics/dist/angulartics.min.js',
                            'angulartics-google-analytics/dist/*.min.js',
                            'angular-bootstrap-checkbox/angular-bootstrap-checkbox.js',
                            'angulartics-facebook-pixel/dist/*.min.js',
                            'ng-dialog/css/ngDialog.min.css',
                            'ng-dialog/css/ngDialog-theme-default.min.css',
                            'ng-dialog/js/ngDialog.min.js'
                        ],
                        dest: 'lib/',
                        expand: true,
                        flatten: true
                    }
                ]
            },
            leaflet: {
                files: [
                    {
                        cwd: 'bower_components/leaflet/dist',
                        src: [
                            'leaflet.js',
                            'leaflet.css',
                            'images/**'
                        ],
                        dest: 'lib/leaflet',
                        expand: true,
                        flatten: false
                    }
                ]
            },
            leaflet_markers: {
                files: [
                    {
                        cwd: 'bower_components/Leaflet.awesome-markers/dist',
                        src: [
                            'leaflet.awesome-markers.min.js',
                            'leaflet.awesome-markers.css',
                            'images/**'
                        ],
                        dest: 'lib/leaflet',
                        expand: true,
                        flatten: false
                    }
                ]
            },
            prod: {
                files: [
                    {
                        src: [
                            'lib/angular-bluebird*',
                            'lib/angular-simple-*',
                            'lib/angular-ui-*',
                            'lib/angular-growl*',
                            'lib/angulartics*.js',
                            'lib/bluebird.min.js',
                            'lib/isteven*',
                            'lib/angular_card*',
                            'lib/lodash.min.js',
                            'lib/ng*',
                            'lib/ui*',
                            'lib/leaflet/**',
                            'lib/moment.min.js',
                            'lib/angular-moment.min.js',
                            'lib/fullcalendar.min.*',
                            'lib/angular-bootstrap-checkbox.js',
                            'lib/modernizr*.js',
                            'content/**',
                            'fonts/**',
                            'app/**/*.html',
                            'app/watchdog.min.js'
                        ],
                        dest: 'dist',
                        expand: true,
                        flatten: false
                    }
                ]
            }
        },
        jscs: {
            main: {
                src: ['app/**/*.js', '!app/*.min.js']
            }
        },
        jshint: {
            options: {
                jshintrc: true
            },
            main: ['app/**/*.js', '!app/*.min.js']
        },
        eslint: {
            src: ['app/**/*.js', '!app/*.min.js']
        },
        less: {
            development: {
                options: {
                    strictMath: true,
                    sourceMap: true,
                    outputSourceFiles: true,
                    sourceMapURL: 'watchdog.css.map',
                    sourceMapFilename: 'dist/css/watchdog.css.map',
                    paths: ["bower_components/bootstrap/less"]
                },
                files: {
                    "content/watchdog.css": "content/watchdog.less"
                }
            }
        },
        cssmin: {
            options: {
                // TODO: disable `zeroUnits` optimization once clean-css 3.2 is released
                //    and then simplify the fix for https://github.com/twbs/bootstrap/issues/14837 accordingly
                compatibility: 'ie8',
                keepSpecialComments: '*',
                advanced: false
            },
            minifyCore: {
                src: 'content/watchdog.css',
                dest: 'content/watchdog.min.css'
            },
            minifyisteven: {
                src: 'bower_components/isteven-angular-multiselect/*.css',
                dest: 'lib/isteven-multi-select.min.css'
            }
        },
        uglify: {
            production: {
                options: {
                    sourceMap: false
                },
                files: {
                    'app/watchdog.min.js': ['app/*.module.js', 'app/**/*.js', '!app/watchdog.min.js']
                }
            },
            isteven: {
                files: {
                    'lib/isteven-multi-select.min.js': ['bower_components/isteven-angular-multiselect/*.js'],
                    'lib/ui-calendar.min.js': ['bower_components/angular-ui-calendar/src/calendar.js'],
                    'lib/angular_card_input.min.js': ['bower_components/angular-card-input/angular_card_input.js']
                }
            }
        },
        karma: {
            unit: {
                options: {
                    frameworks: ['jasmine'],
                    singleRun: true,
                    browsers: ['PhantomJS'],
                    files: [
                        'lib/angular.min.js',
                        'bower_components/angular-mocks/angular-mocks.js',
                        'app/**/*.js',
                        'spec/**/*.js'
                    ]
                }
            }
        },
        watch: {
            less: {
                files: ['content/**/*.less'],
                tasks: ['less'],
                options: {
                    spawn: false
                }
            }
        },
        processhtml: {
            dist: {
                files: {
                    'dist/index.html': ['index.html']
                }
            }
        },
        html2js: {
            options: {
                htmlmin: {
                    collapseBooleanAttributes: true,
                    collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeComments: true,
                    removeEmptyAttributes: true,
                    removeRedundantAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true
                },
                base: './'
            },
            main: {
                src: ['app/components/**/*.html'],
                dest: 'dist/app/templates.js'
            }
        },
        clean: {
            dist: ['dist']
        },
        compress : {
            main : {
                options : {
                    archive : "dashboard.zip"
                },
                files : [
                    { expand: true, src : "**/*", cwd : "dist/" }
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks("gruntify-eslint");
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-jscs');
    grunt.loadNpmTasks('grunt-processhtml');
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-contrib-compress');

    grunt.registerTask('default',
        [
            'uglify:isteven',
            'cssmin:minifyisteven',
            'less',
            'jscs',
            'jshint',
            'eslint',
            'copy:main',
            'copy:leaflet',
            'copy:leaflet_markers'
        ]);

    grunt.registerTask(
        'prod',
        [
            'clean:dist',
            'uglify',
            'cssmin',
            'default',
            'copy:prod',
            'html2js',
            'processhtml',
            'compress'
        ]);

};
