(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .filter('wdDate', WdDate);

    WdDate.$inject = ['$filter', 'moment'];

    function WdDate($filter, moment) {
        var filter = $filter('amDateFormat');

        return function (input) {
            if (!input) {
                return 'Unknown';
            }

            return filter(moment.utc(input).toDate(), 'YYYY-MM-DD h:mm:ss A');
        };
    }
}());
