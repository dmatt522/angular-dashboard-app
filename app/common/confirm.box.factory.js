(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('confirmBox', ConfirmBox);

    ConfirmBox.$inject = ['$uibModal', '$sce'];

    function ConfirmBox($uibModal, $sce) {
        return {
            showDelete: showDelete,
            showYesNo: showYesNo
        };

        function showDelete(title, message) {
            return showBox(title, message, 'Delete', 'Cancel');
        }

        function showYesNo(title, message) {
            return showBox(title, message, 'Yes', 'No');
        }

        function showBox(title, message, okLabel, cancelLabel) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/common/confirm-box.html',
                controller: ConfirmController,
                controllerAs: 'ctrl',
                size: 'md',
                resolve: {
                    message: function () {
                        return $sce.trustAsHtml(message);
                    },
                    title: function () {
                        return $sce.trustAsHtml(title);
                    },
                    okLabel: function () {
                        return okLabel;
                    },
                    cancelLabel: function () {
                        return cancelLabel;
                    }
                }
            });

            return modalInstance.result;
        }
    }

    ConfirmController.$inject = ['$uibModalInstance', 'title', 'message', 'okLabel', 'cancelLabel'];

    function ConfirmController($uibModalInstance, title, message, okLabel, cancelLabel) {
        var vm = this;

        vm.title = title;
        vm.message = message;
        vm.okLabel = okLabel;
        vm.cancelLabel = cancelLabel;

        vm.ok = function () {
            $uibModalInstance.close(true);
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
}());
