/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .directive('accountBar', accountBar);

    accountBar.$inject = [];

    /* @ngInject */
    function accountBar() {
        var directive = {
            bindToController: true,
            controller: AccountBarCtrl,
            controllerAs: 'vm',
            link: link,
            restrict: 'A',
            templateUrl: 'app/common/account.bar.directive.html'
        };
        return directive;

        function link(scope, element, attrs) {
            element.addClass('accountsbar layout-row');

            if (attrs.navigate) {
                scope.vm.navigate = attrs.navigate;
            }
        }
    }

    AccountBarCtrl.$inject = ['$scope'];

    /* @ngInject */
    function AccountBarCtrl($scope) {
        var vm = this;

        vm.selectDevice = selectDevice;

        function selectDevice(device) {
            $scope.secure.selectDevice(device);

            if (vm.navigate) {
                if (device.is_configured) {
                    $scope.$state.go(vm.navigate, null, {reload: true});
                } else {
                    // If the device is not configured, always navigate to secure.dashboard
                    $scope.$state.go('secure.dashboard', null, {reload: true});
                }
            }
        }
    }
})();
