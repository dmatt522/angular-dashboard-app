(function () {
    'use strict';

    angular
        .module(
            'WatchDogApp',
            [
                'ui.router',
                'ngResource',
                'ngMessages',
                'ngAnimate',
                'ngCookies',
                'ui.bootstrap',
                'ngtimeago',
                'mwl.bluebird',
                'ui-leaflet',
                'infinite-scroll',
                'ngFileUpload',
                'templates-main',
                'isteven-multi-select',
                'angularMoment',
                'angular-growl',
                'creditCardInput',
                'angulartics',
                'angulartics.google.analytics',
                'angulartics.facebook.pixel',
                'ui.checkbox',
                'ngDialog'
            ]);
}());
