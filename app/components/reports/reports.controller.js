/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('ReportsController', ReportsCtrl);

    ReportsCtrl.$inject = ['dataService', 'SpinnerService', 'session', '$uibModal'];

    function ReportsCtrl(dataService, SpinnerService, session,$uibModal) {
        var vm = this, defaultFilters = ['3', '7', '2', '9,10', '8'], defaultDate = '1w', templates = {
            none: 'app/components/reports/report-blank.html',
            details: 'app/components/reports/report-details.html'
        };
        vm.template = templates.none;

        vm.selectFilterData = [
            {icon: wrapNavIcon('applications.svg'), name: 'Apps', id: '6', ticked: true},
            {icon: wrapNavIcon('calls.svg'), name: 'Calls', id: '3', ticked: true},
            {icon: wrapNavIcon('websites.svg'), name: 'Websites', id: '7', ticked: true},
            {icon: wrapNavIcon('contacts.svg'), name: 'Contacts', id: '2', ticked: true},
            {icon: wrapNavIcon('messages.svg'), name: 'Messages', id: '9,10', ticked: true},
            {icon: wrapNavIcon('locations.svg'), name: 'Locations', id: '8', ticked: true},
            {icon: wrapNavIcon('device-red.svg'), name: 'Devices', id: '1', ticked: true}
        ];

        vm.selectDateData = [
            {icon: wrapClock(), name: 'Last Week', duration: '1w', ticked: true},
            {icon: wrapClock(), name: 'Two Weeks', duration: '2w', ticked: false},
            {icon: wrapClock(), name: 'One Month', duration: '1m', ticked: false},
            {icon: wrapClock(), name: 'Three Months', duration: '3m', ticked: false},
            {icon: wrapClock(), name: 'Six Months', duration: '6m', ticked: false},
            {icon: wrapClock(), name: 'One Year', duration: '1y', ticked: false},
            {icon: wrapClock(), name: 'All', duration: 'all', ticked: false}
        ];

        vm.typeResMap = {
            1: {icon: 'device-f.svg', template: 'device-template.html'},
            2: {icon: 'contacts.svg', template: 'contact-template.html'},
            3: {icon: 'icons_calls.svg', template: 'call-template.html'},
            6: {icon: 'icons_applications.svg', template: 'app-template.html'},
            7: {icon: 'websites.svg', template: 'website-template.html'},
            8: {icon: 'locations.svg', template: 'location-template.html'},
            9: {icon: 'icons_messages.svg', template: 'message-template.html'}
        };

        activate();

        vm.ltClick = ltClick;
        vm.ltSelectAll = ltSelectAll;
        vm.ltSelectNone = ltSelectNone;
        vm.dClick = dClick;
        vm.selectLog = selectLog;
        vm.closeLog = closeLog;
        vm.createReport = openDisclaimerModal;
        vm.currentDetailsTemplate = '';
        vm.addEditNote = addEditNote;

        function activate() {
            var data = {
                device_id: session.deviceId,
                log_type_id: '[' + defaultFilters.join(',') + ']',
                date_offset: defaultDate
            };
            fetchResults(data);
        }

        function fetchResults(criteria) {
            if (criteria.log_type_id !== '[]') {
                SpinnerService.show();
                dataService.reports(criteria)
                    .then(function (result) {
                        vm.results = result.logs;
                    })
                    .finally(SpinnerService.hide);
            }
        }

        function wrapNavIcon(icon) {
            return wrapIcon('./content/images/navigation_icons/' + icon);
        }

        function wrapClock() {
            return wrapIcon('./content/images/icons/clock.svg');
        }

        function wrapIcon(src) {
            return '<img style="border-radius: 50%;width: 20px; height: auto;" src="' + src + '">';
        }

        function fetchLogTypes() {
            var logTypes = [];
            angular.forEach(vm.outputFilters, function (data) {
                if (data.ticked) {
                    logTypes.push(data.id);
                }
            }, logTypes);
            return logTypes;
        }

        function fetchDateType() {
            var dateType = defaultDate;
            angular.forEach(vm.outputDate, function (data) {
                if (data.ticked) {
                    dateType = data.duration;
                }
            }, dateType);
            return dateType;
        }

        function getCriteria() {
            return {
                device_id: session.deviceId,
                log_type_id: '[' + fetchLogTypes().join(',') + ']',
                date_offset: fetchDateType()
            };
        }

        function ltClick(data) {
            fetchResults(getCriteria());
        }

        function ltSelectAll() {
            fetchResults(getCriteria());
        }

        function ltSelectNone() {
            vm.results = undefined;
        }

        function dClick(data) {
            fetchResults(getCriteria());
        }

        function selectLog(log) {
            vm.selectedRow = log;
            selectTemplate(log.log_type_id);
            vm.template = templates.details;
        }

        function closeLog() {
            vm.template = templates.none;
            vm.selectedRow = undefined;
        }

        function selectTemplate(type) {
            vm.currentDetailsTemplate = vm.typeResMap[type].template;
        }

        function openDisclaimerModal(logItem) {
            SpinnerService.show();
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/components/reports/create-report-disclaimer-modal.html',
                size: 'md',
                controller: 'CreateReportsDisclaimerController',
                controllerAs: 'ctrl',
                resolve: {
                }
            });

            modalInstance.opened.then(function () {
                SpinnerService.hide();
            });

            modalInstance.result.then(function (result) {
                if (result) {
                    openReportModal(logItem);
                }
            }, function () {
            });
        }

        function addEditNote(logItem) {
            SpinnerService.show();
            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'app/components/reports/create-note-modal.html',
                size: 'md',
                controller: 'CreateNoteController',
                controllerAs: 'ctrl',
                resolve: {
                    logItem: function () {
                        return logItem;
                    }
                }
            });

            modalInstance.opened.then(function () {
                SpinnerService.hide();
            });

            modalInstance.result.then(function (note) {
                logItem.note = note;
            });
        }

        function openReportModal(logItem) {
            SpinnerService.show();
            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'app/components/reports/create-report-modal.html',
                size: 'md',
                controller: 'CreateReportsController',
                controllerAs: 'ctrl',
                resolve: {
                    logItem: function () {
                        return logItem;
                    }
                }
            });

            modalInstance.opened.then(function () {
                SpinnerService.hide();
            });
        }
    }
}());
