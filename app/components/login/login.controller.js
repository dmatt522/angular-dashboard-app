/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('LoginController', LoginCtrl);

    LoginCtrl.$inject = ['$scope', 'userService', 'session', 'dataService', 'SpinnerService', 'growl', '$log', '$window'];

    function LoginCtrl($scope, userService, session, dataService, SpinnerService, growl, $log, $window) {
        var vm = this;

        vm.email = '';
        vm.password = '';

        vm.login = login;
        vm.forgotSubmit = forgotSubmit;

        activate();

        function activate() {
            session.loggedIn = false;

            try {
                localStorage.test = 2;
                vm.email = localStorage.getItem('email');
            } catch (e) {
                $log.warn('Unable to read localStorage');

                growl.error('This site doesn\'t work in Private Browsing mode. ' +
                    '<br>Please turn off Private Browsing and reload this page.', {
                    title: 'Is Private Browsing turned on?',
                    ttl: 10000
                });
            }

            if (vm.email) {
                vm.rememberMe = true;
            }
        }

        function login() {
            SpinnerService.show();

            session.deviceId = undefined;

            userService
                .login(vm.email, vm.password)
                .then(function (response) {
                    $window.console.log('login controller response', response);
                    session.loggedIn = true;
                    session.isActiveSubscriber = response.content.is_paid;

                    if (vm.rememberMe) {
                        try {
                            localStorage.setItem('email', vm.email);
                        } catch (e) {
                            $log.warn('Unable to set email');
                        }
                    }
                    session.login_user_email = vm.email;
                    $scope.$emit('userChange', vm.email);
                    // $window.alert('isActiveSubscriber = ' + session.isActiveSubscriber);

                    if (session.isActiveSubscriber) {
                        $scope.$state.go('secure.dashboard');
                    } else {
                        $scope.$state.go('secure.settings.billing');
                    }
                    // $scope.$state.go('secure.settings.billing');
                })
                .catch(function (error) {
                    $log.log(error);
                    growl.error('Unable to log you in. Please verify your credentials or turn off Private Browsing.', {
                        title: 'Error on Log In',
                        ttl: 10000
                    });
                })
                .finally(SpinnerService.hide);
        }

        function forgotSubmit() {
            SpinnerService.show();

            dataService
                .account
                .forgotPassword(vm.email)
                .then(function () {
                    $scope.$state.go('forgot-success');
                })
                .finally(SpinnerService.hide);
        }
    }
}());
