﻿/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('AlertsController', AlertsCtrl);

    AlertsCtrl.$inject = ['dataService', 'alertAddressData', '$scope', 'SpinnerService', 'confirmBox', 'growl'];

    function AlertsCtrl(dataService, alertAddressData, $scope, SpinnerService, confirmBox, growl) {
        var vm = this;

        vm.delete = deleteAddress;
        vm.updateAlerts = updateAlerts;
        vm.updateDailyWatch = updateDailyWatch;
        vm.reSend = reSendLink;

        activate();

        function activate() {
            SpinnerService.show();

            loadAlertAddresses()
                .then(function () {
                    return dataService
                        .accounts()
                        .then(function (response) {
                            vm.account = response;
                        });
                })
                .finally(SpinnerService.hide);
        }

        function deleteAddress(address) {
            confirmBox.showDelete('Delete Address', 'Are you sure you want to delete <em>' + address.email + '</em>?')
                .then(function () {
                    return alertAddressData.remove(address.id);
                })
                .then(function (response) {
                    if (response) {
                        loadAlertAddresses();

                        growl.success('Alert has been deleted', {ttl: 5000});
                    }
                });
        }

        function updateAlerts() {
            dataService.account.update(undefined, vm.account.is_alerts_enabled);
        }

        function updateDailyWatch() {
            dataService.account.update(undefined, undefined, vm.account.is_daily_watch_enabled);
        }

        function loadAlertAddresses() {
            return alertAddressData
                .list()
                .then(function (response) {
                    vm.alertAddresses = response;
                });
        }

        function reSendLink(address) {
            address
                .$update()
                .then(function (response) {
                    growl.success('Validation link has been requested', {ttl: 5000});

                    return loadAlertAddresses();
                })
                .catch(function (err) {
                    growl.error('Unable to send validation link', {ttl: 10000});
                });
        }
    }
}());
