﻿/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('DeviceListController', DeviceListCtrl);

    DeviceListCtrl.$inject = ['dataService', '$scope', 'confirmBox', 'growl', 'SpinnerService'];

    function DeviceListCtrl(dataService, $scope, confirmBox, growl, SpinnerService) {
        var vm = this;

        vm.resendLink = resendLink;
        vm.sync = sync;
        vm.delete = deleteDevice;

        activate();

        function activate() {
            SpinnerService.show();

            dataService
                .devices()
                .then(function (response) {
                    vm.devices = response.content;
                })
                .finally(SpinnerService.hide);
        }

        function resendLink(deviceId) {
            var message = 'This will send a text message with a link to your child\'s device.<br>' +
                'Click the link to download the My Mobile Watchdog child app.';
            confirmBox
                .showYesNo('Resend Configuration Link',
                    '<p>' + message + '</p><p><strong>Are you still sure you want to send it?</strong></p>')
                .then(function (result) {
                    if (result) {
                        dataService
                            .sendSmsLink(deviceId)
                            .then(function (response) {
                                growl.success('Configuration Link Sent', {title: 'Send Link', ttl: 5000});
                            })
                            .catch(function (err) {
                                growl.error('Unable to resend the link right now, please try again later.', {
                                    title: 'Send Link',
                                    ttl: 10000
                                });
                            });
                    }
                });
        }

        function sync(device) {
            var androidSync = 'This rarely needs to be done. Do this if the ' +
                    'contacts or applications are not in sync with the device',
                iOSSync = 'This will pull an iCloud backup and sync the data.',
                message = device.is_android ? androidSync : iOSSync;
            confirmBox
                .showYesNo('Sync Device',
                    '<p>' + message + '</p><p><strong>Are you still sure you want to sync this device?</strong></p>')
                .then(function (result) {
                    if (result) {
                        dataService
                            .sync(device.id)
                            .then(function (response) {
                                growl.success('The sync has been requested', {title: 'Sync Device', ttl: 5000});
                            });
                    }
                });
        }

        function deleteDevice(deviceId) {
            confirmBox
                .showDelete('Delete Device',
                    '<p><strong>Are you sure you want to delete this device?</strong></p>' +
                    '<p>Once deleted you will not be able to recover the data.</p>')
                .then(function (result) {
                    if (result) {
                        dataService
                            .device
                            .remove(deviceId)
                            .then(function (response) {
                                growl.success('The device has been deleted', {title: 'Delete Device', ttl: 5000});

                                // This is processed in MainController
                                $scope.$emit('deviceChange');

                                $scope.$state.go('secure.settings.devices', {}, {reload: true});
                            });
                    }
                });
        }
    }
}());
