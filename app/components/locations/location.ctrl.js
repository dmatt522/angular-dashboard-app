/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('LocationController', LocationCtrl);

    LocationCtrl.$inject = [
        '$scope', 'markers', 'InfiniteScroll', 'session', 'locationService', '$interval', '_', 'SpinnerService'];

    function LocationCtrl($scope, markers, InfiniteScroll, session, locationService, $interval, _, SpinnerService) {
        var vm = this,
            allMarkers,
            interval;

        vm.markers = allMarkers = markers;
        vm.container = angular.element('#location-container');
        vm.pager = new InfiniteScroll('locations', {device_id: session.deviceId});
        vm.selectLocation = selectLocation;
        vm.showAll = showAll;
        vm.animate = animate;
        vm.stopAnimate = stopAnimate;
        vm.animating = false;
        vm.lastLocation = lastLocation;

        activate();

        function activate() {
            var marker;

            if (markers.length) {
                marker = markers[0];

                vm.center = {
                    lat: Number(marker.lat),
                    lng: Number(marker.lng),
                    zoom: 14
                };

                vm.markers = _.take(allMarkers, 10);
            }
        }

        /**
         * Changes the map center.
         */
        function selectLocation(location) {
            vm.markers = [
                locationService.createMarker(location, true)
            ];

            vm.selected = location;

            vm.center = {
                lat: Number(location.latitude),
                lng: Number(location.longitude),
                zoom: 18
            };
        }

        function showAll() {
            vm.markers = _.take(allMarkers, 10);
            vm.selected = undefined;

            stopAnimate();
        }

        function animate() {
            var next = 0;

            if (vm.animating) {
                stopAnimate();

                return;
            }

            vm.animating = true;

            interval = $interval(function () {
                selectLocation(vm.pager.results[next]);

                next = next + 1;
            }, 1000, vm.pager.results.length);
        }

        function stopAnimate() {
            vm.animating = false;

            $interval.cancel(interval);
        }

        function lastLocation() {
            if (!$scope.$state.is('secure.location.all')) {
                $scope.$state.go($scope.$state.current, {}, {reload: true});
            } else {
                SpinnerService.show();

                locationService
                    .lastLocation(session.deviceId)
                    .then(function (markers) {
                        $scope.$state.go($scope.$state.current, {}, {reload: true});
                    })
                    .finally(SpinnerService.hide);
            }
        }
    }
}());
