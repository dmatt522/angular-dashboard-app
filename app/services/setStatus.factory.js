/*jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('setStatus', SetStatus);

    SetStatus.$inject = ['dataService', 'session', '$log', '$window'];

    function SetStatus(dataService, session, $log, $window) {
        var setStatus = {},
            deviceId = session.deviceId;

        setStatus.updateStatus = function (scope, page, status) {
            if (page) {
                $window.console.log('setStatus.updateStatus page', page);
                $window.console.log('setStatus.updatestatus status', status);
                switch (page) {
                    case 'applications':
                        updateApplication(scope, status);
                        break;
                    case 'websites':
                        updateWebsite(scope, status);
                        break;
                    case 'contacts':
                        updateContact(scope, status);
                        break;
                    case 'timeblocks':
                        updateTimeBlock(scope, status);
                        break;
                }
            }
        };

        function updateApplication(scope, status) {
            var alertStatusId = 1, restrictionStatusId = 1;

            switch (status) {
                case 'allow':
                    restrictionStatusId = 1;
                    break;
                case 'temp_allow':
                    restrictionStatusId = 2;
                    break;
                case 'block':
                    restrictionStatusId = 3;
                    break;
                case 'disable':
                    restrictionStatusId = 4;
                    break;
            }

            dataService
                .application
                .updateStatus(scope.rowData.id, alertStatusId, restrictionStatusId)
                .then(function (result) {
                    $log.log('Successfully updated Application: ' + result);
                    scope.rowData.restriction_status_id = restrictionStatusId;
                });
        }

        function updateTimeBlock(scope, status) {
            $window.console.log('scope.rowData', scope.rowData);
            $window.console.log('status', status);

            var timeBlockId = scope.rowData.id,
                deviceId = scope.rowData.device_id;

            switch (status) {
                case 'enable':
                    dataService
                        .timeblock
                        .updateStatus(deviceId, timeBlockId, true)
                        .then(function (result) {
                            $log.log('Successfully enabled time block ' + scope.rowData.id);
                            scope.rowData.is_enabled = true;
                            scope.$emit('updatePagerItem', scope.rowData);
                        });
                    return;
                case 'disable':
                    dataService
                        .timeblock
                        .updateStatus(deviceId, timeBlockId, false)
                        .then(function (result) {
                            $log.log('Successfully disabled time block ' + scope.rowData.id);
                            scope.rowData.is_enabled = false;
                            scope.$emit('updatePagerItem', scope.rowData);
                        });
                    return;
                case 'delete':
                    dataService
                        .timeblock
                        .remove(deviceId, timeBlockId)
                        .then(function (result) {
                            $log.log('Successfully deleted time block ' + scope.rowData.id);
                            scope.$emit('removePagerItem', scope.rowData);
                        });
                    return;
            }

            // dataService
            //    .contact
            //    .updateStatus(timeBlockId, alertStatusId, restrictionStatusId)
            //    .then(function (result) {
            //        $log.log('Successfully updated time block');
            //        scope.rowData.device_contact.alert_status_id = alertStatusId;
            //    });
        }

        function updateContact(scope, status) {
            var alertStatusId = 1,
                restrictionStatusId = 1,
                contactId = scope.rowData.device_contact.id;

            switch (status) {
                case 'alert':
                    alertStatusId = 2;
                    break;
                case 'allow':
                    alertStatusId = 1;
                    break;
                case 'delete':
                    dataService
                        .contact
                        .remove(contactId)
                        .then(function (result) {
                            $log.log('Successfully deleted Contact' + scope.rowData.device_contact.id);
                            scope.$emit('removePagerItem', scope.rowData);
                        });

                    return;
            }

            dataService
                .contact
                .updateStatus(contactId, alertStatusId, restrictionStatusId)
                .then(function (result) {
                    $log.log('Successfully updated Contact');
                    scope.rowData.device_contact.alert_status_id = alertStatusId;
                });
        }

        function updateWebsite(scope, status) {
            var alertStatusId = 1,
                restrictionStatusId = 1,
                websiteId = scope.rowData.id;

            switch (status) {
                case 'alert':
                    alertStatusId = 2;
                    restrictionStatusId = 1;
                    break;
                case 'allow':
                    alertStatusId = 1;
                    restrictionStatusId = 1;
                    break;
                case 'block':
                    alertStatusId = 1;
                    restrictionStatusId = 3;
                    break;
                case 'delete':
                    dataService
                        .website
                        .remove(websiteId)
                        .then(function (result) {
                            $log.log('Successfully deleted Website' + scope.rowData.id);
                            scope.$emit('removePagerItem', scope.rowData);
                        });

                    return;
            }

            dataService
                .website
                .updateStatus(websiteId, alertStatusId, restrictionStatusId)
                .then(function (result) {
                    $log.log('Successfully updated Website');
                    scope.rowData.alert_status_id = alertStatusId;
                    scope.rowData.restriction_status_id = restrictionStatusId;
                });
        }

        return setStatus;
    }
}());
