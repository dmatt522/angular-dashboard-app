(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('actions', Factory);

    Factory.$inject = ['ErrorResponseInterceptor'];

    function Factory(interceptor) {
        return {
            get: {method: 'GET', withCredentials: true, interceptor: interceptor},
            add: {method: 'POST', withCredentials: true, interceptor: interceptor},
            update: {method: 'PUT', withCredentials: true, interceptor: interceptor},
            query: {
                method: 'GET',
                isArray: true,
                withCredentials: true,
                interceptor: interceptor,
                transformResponse: transformResponse
            },
            remove: {method: 'DELETE', withCredentials: true, interceptor: interceptor}
        };

        function transformResponse(data) {
            var response = angular.fromJson(data);

            // This way we can just update the object and call update
            return response.content;
        }
    }
}());
