/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('billingData', BillingDataFactory);

    BillingDataFactory.$inject = ['appConfig', '$resource', 'actions', 'dataServiceSimple', '$window'];

    function BillingDataFactory(appConfig, $resource, actions, dataService, $window) {
        var endpoint = $resource(appConfig.apiUrl + '/billings', undefined, actions);

        return {
            get: billings,
            save: save,
            update: update,
            saveOrUpdate: saveOrUpdate
        };

        function billings() {
            return endpoint.get().$promise;
        }

        function saveOrUpdate(card, cvc, expMonth, expYear, firstName, lastName, address1, city, state, zip, oneTimePayment, promo, billing) {
            // this needs to be a true boolean
            if (oneTimePayment === 'true') {
                oneTimePayment = true;
            } else if (oneTimePayment === 'false') {
                oneTimePayment = false;
            }
            if (angular.isDefined(billing.source)) {
                // check if we have previously defined a billing source
                if (angular.isDefined(billing.source) && angular.isDefined(billing.source.billing_source_id)) {
                    return update(card, cvc, expMonth, expYear, firstName, lastName, address1, city, state, zip, oneTimePayment);
                }
            }
            return save(card, cvc, expMonth, expYear, firstName, lastName, address1, city, state, zip, promo, oneTimePayment);
        }

        // we can probably combine the following 2 methods... :)

        function update(card, cvc, expMonth, expYear, firstName, lastName, address1, city, state, zip, oneTimePayment) {
            var args = {
                    card_number: card,
                    cv_number: cvc,
                    expiration_month: expMonth,
                    expiration_year: expYear,
                    first_name: firstName,
                    last_name: lastName,
                    street1: address1,
                    city: city,
                    state: state,
                    postal_code: zip,
                    one_time_payment: oneTimePayment,
                    country: 'US'
                };
            $window.console.log('billing update', args);
            return dataService.update('billings', args);
        }

        function save(card, cvc, expMonth, expYear, firstName, lastName, address1, city, state, zip, promoCode, oneTimePayment) {
            var args = {
                    card_number: card,
                    cv_number: cvc,
                    expiration_month: expMonth,
                    expiration_year: expYear,
                    first_name: firstName,
                    last_name: lastName,
                    street1: address1,
                    city: city,
                    state: state,
                    postal_code: zip,
                    one_time_payment: oneTimePayment,
                    promo_code: promoCode,
                    country: 'US'
                };
            $window.console.log('billing save', args);
            return endpoint
                .save(args).$promise;
        }
    }
}());
