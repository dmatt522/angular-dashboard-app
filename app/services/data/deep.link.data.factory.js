/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('DeepLinkData', DeepLinkData);

    DeepLinkData.$inject = ['appConfig', '$resource'];

    /* @ngInject */
    function DeepLinkData(appConfig, $resource) {
        var service, deepLinkResource = $resource(appConfig.apiUrl + '/deeplink/:id', {id: '@id'});

        service = {
            read: read
        };

        return service;

        function read(id) {
            return deepLinkResource.get({id: id}).$promise;
        }
    }
})();
