/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('callsData', CallsDataFactory);

    CallsDataFactory.$inject = ['dataServiceSimple'];

    function CallsDataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'calls';

        dataFactory.calls = calls;

        return dataFactory;

        function calls(offset, limit, deviceId) {
            var args = {
                offset: offset,
                limit: limit,
                device_id: deviceId
            };
            return dataService.get(endpoint, args);
        }
    }
}());
