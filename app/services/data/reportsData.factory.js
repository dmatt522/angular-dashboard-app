/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('reportsData', ReportsDataFactory);

    ReportsDataFactory.$inject = ['dataServiceSimple'];

    function ReportsDataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'reports';

        dataFactory.reports = reports;

        return dataFactory;

        function reports(args) {
            return dataService.get(endpoint, args);
        }
    }
}());
