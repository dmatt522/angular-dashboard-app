/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('dashboardData', DashboardDataFactory);

    DashboardDataFactory.$inject = ['dataServiceSimple'];

    function DashboardDataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'dashboard';

        dataFactory.dashboard = function () {
            return dataService.get(endpoint);
        };

        return dataFactory;
    }
}());
