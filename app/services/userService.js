﻿/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('userService', UserService);

    UserService.$inject = ['appConfig', '$resource'];

    function UserService(appConfig, $resource) {
        var userService = {},
            sessionResource;

        sessionResource = $resource(appConfig.apiUrl + '/session',
            {},
            {
                login: {
                    method: 'POST',
                    withCredentials: true
                },
                logout: {
                    method: 'DELETE',
                    withCredentials: true
                }
            });

        userService.login = function (userName, password) {
            return sessionResource
                .login({username: userName, password: password})
                .$promise
                .then(function (response) {
                    if (response.content && response.content.account_id) {
                        userService.isLoggedIn = true;
                        userService.userInfo = response.content;
                    }

                    return response;
                });
        };

        userService.logout = function () {
            return sessionResource
                .logout()
                .$promise;
        };

        return userService;
    }
}());
